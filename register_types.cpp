#include "precompiled_ui.hpp"

#include "register_types.h"

#include "bwn_ui/notifications/uiNotificationManager.hpp"
#include "bwn_ui/notifications/uiNotification.hpp"

#include "bwn_ui/menus/menu.hpp"
#include "bwn_ui/menus/menuManager.hpp"
#include "bwn_ui/menus/operations/changeMenuOperation.hpp"
#include "bwn_ui/menus/operations/hideMenuOperation.hpp"
#include "bwn_ui/menus/operations/showMenuOperation.hpp"
#include "bwn_ui/menus/notifications/menuChangedNotification.hpp"

void initialize_bwn_ui_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SCENE)
	{
		GDREGISTER_CLASS(bwn::UiNotification);
		GDREGISTER_CLASS(bwn::UiNotificationManager);

		GDREGISTER_CLASS(bwn::Menu);
		GDREGISTER_CLASS(bwn::MenuManager);
		GDREGISTER_CLASS(bwn::ChangeMenuOperation);
		GDREGISTER_CLASS(bwn::HideMenuOperation);
		GDREGISTER_CLASS(bwn::ShowMenuOperation);

		GDREGISTER_CLASS(bwn::MenuChangedNotification);
	}
}

void uninitialize_bwn_ui_module(const ModuleInitializationLevel)
{}
#pragma once

#include "bwn_notifications/notificationManagerBase.hpp"
#include "bwn_core/types/singleton.hpp"

namespace bwn
{
	class UiNotificationManager : public NotificationManagerBase, public AutoSingleton<UiNotificationManager>
	{
		GDCLASS(UiNotificationManager, NotificationManagerBase);

		//
		// Construction and destruction.
		//
	public:
		UiNotificationManager();
		virtual ~UiNotificationManager() override;
	};
} // namespace bwn
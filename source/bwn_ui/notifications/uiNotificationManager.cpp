#include "precompiled_ui.hpp"

#include "bwn_ui/notifications/uiNotificationManager.hpp"

bwn::UiNotificationManager::UiNotificationManager()
	: NotificationManagerBase( Node::NOTIFICATION_PROCESS )
{
	set_process_priority(1);
	set_process(true);
}

bwn::UiNotificationManager::~UiNotificationManager() = default;
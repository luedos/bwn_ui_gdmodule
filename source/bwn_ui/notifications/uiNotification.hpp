#pragma once

#include "bwn_notifications/notification.hpp"
#include "bwn_ui/notifications/uiNotificationManager.hpp"

namespace bwn
{
	class UiNotification : public NotificationInterface<UiNotification, Notification<UiNotificationManager>>
	{
		GDCLASS(UiNotification, NotificationBase);

		//
		// Construction and destruction.
		//
	public:
		// Simply propagating construction from interface.
		using NotificationInterface::NotificationInterface;

		//
		// Godot classes.
		//
	protected:
		static void _bind_methods();
	};

	extern template class NotificationInterface<UiNotification, Notification<UiNotificationManager>>;
} // namespace bwn
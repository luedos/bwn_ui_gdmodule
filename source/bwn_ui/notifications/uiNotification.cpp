#include "precompiled_ui.hpp"

#include "bwn_ui/notifications/uiNotification.hpp"
#include "bwn_notifications/notificationRegistry.hpp"

void bwn::UiNotification::_bind_methods()
{
	NotificationRegistry::getInstance()->registerNotification<UiNotification>();
}

template class bwn::NotificationInterface<bwn::UiNotification, bwn::Notification<bwn::UiNotificationManager>>;
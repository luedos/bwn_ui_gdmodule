#pragma once

#include "bwn_ui/notifications/uiNotification.hpp"

class Node;

namespace bwn
{
	class Menu;

	class MenuChangedNotification : public NotificationInterface<MenuChangedNotification, UiNotification>
	{
		GDCLASS(MenuChangedNotification, UiNotification);

		//
		// Construction and destruction.
		//
	public:
		MenuChangedNotification();
		MenuChangedNotification(Menu*const _menu, const bool _shown, const bool _aborted);

		//
		// Godot classes.
		//
	private:
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		Menu* getMenu() const;
		bool isShown() const;
		bool isAborted() const;

		//
		// Private members.
		//
	private:
		Menu* m_menu = nullptr;
		bool m_aborted = false;
		bool m_shown = false;
	};
} // namespace bwn
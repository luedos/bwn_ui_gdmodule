#include "precompiled_ui.hpp"

#include "bwn_ui/menus/notifications/menuChangedNotification.hpp"
#include "bwn_ui/menus/menu.hpp"

#include "bwn_notifications/notificationRegistry.hpp"

bwn::MenuChangedNotification::MenuChangedNotification() = default;

bwn::MenuChangedNotification::MenuChangedNotification(Menu*const _menu, const bool _shown, const bool _aborted)
	: m_menu( _menu )
	, m_aborted( _aborted )
	, m_shown( _shown )
{}

void bwn::MenuChangedNotification::_bind_methods()
{
	ClassDB::bind_method(D_METHOD("get_menu"), &MenuChangedNotification::getMenu);
	ClassDB::bind_method(D_METHOD("is_aborted"), &MenuChangedNotification::isAborted);
	ClassDB::bind_method(D_METHOD("is_shown"), &MenuChangedNotification::isShown);

	NotificationRegistry::getInstance()->registerNotification<MenuChangedNotification>();
}

bwn::Menu* bwn::MenuChangedNotification::getMenu() const
{
	return m_menu;
}

bool bwn::MenuChangedNotification::isAborted() const
{
	return m_aborted;
}

bool bwn::MenuChangedNotification::isShown() const
{
	return m_shown;
}
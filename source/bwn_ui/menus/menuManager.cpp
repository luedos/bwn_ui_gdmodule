#include "precompiled_ui.hpp"

#include "bwn_ui/menus/menuManager.hpp"
#include "bwn_ui/menus/menu.hpp"
#include "bwn_ui/menus/operations/changeMenuOperation.hpp"
#include "bwn_coroutines/coroutineContext.hpp"

#include <scene/main/window.h>
#include <modules/modules_enabled.gen.h>

#if defined(MODULE_BWN_CONSOLE_ENABLED)
	#include "bwn_console/consoleDatabase.hpp"
#endif

bwn::MenuManager::MenuManager()
	: m_default( nullptr )
	, m_stack()
	, m_channel()
	, m_context()
{
	m_context.instantiate();

#if defined(DEBUG_ENABLED)
	m_channel.setParentCreatorInfo(std::make_shared<bwn::debug::CoroutineCreatorInfo>("MenuManager"));
#endif
}

bwn::MenuManager::~MenuManager()
{
	m_context->setCallback(nullptr);
}

void bwn::MenuManager::_bind_methods()
{	
	bwn::bindMethod(D_METHOD("set_default_menu"), &MenuManager::resetDefaultMenuGeneric);

	bwn::bindMethod(D_METHOD("push"), &MenuManager::pushGeneric);
	bwn::bindMethod(D_METHOD("pop_last"), &MenuManager::popLast);
	bwn::bindMethod(D_METHOD("pop_till"), &MenuManager::popTillGeneric);
	bwn::bindMethod(D_METHOD("pop_list"), &MenuManager::popListGeneric);
	bwn::bindMethod(D_METHOD("pop_specific"), &MenuManager::popSpecificGeneric);
	bwn::bindMethod(D_METHOD("pop_all_stack"), &MenuManager::popAllStack);
}

void bwn::MenuManager::_notification(int _notification)
{
	// on entry to tree, register singleton
	// on exit tree, unregister singleton

	switch (_notification)
	{
		case NOTIFICATION_ENTER_TREE:
		{
			initSingleton(this);

#if defined(DEBUG_ENABLED) && defined(MODULE_BWN_CONSOLE_ENABLED)
			const auto database = ConsoleDatabase::getInstance();
			database->addMethod(
				U"push_menu",
				bwn::wrap_callback(this, &MenuManager::push));
			database->addMethod(
				U"pop_till_menu",
				bwn::wrap_callback(this, &MenuManager::popTill));
			database->addMethod(
				U"pop_list_menu",
				bwn::wrap_callback(this, &MenuManager::popList));
			database->addMethod(
				U"pop_specific_menu",
				bwn::wrap_callback(this, &MenuManager::popSpecific));
			database->addMethod(
				U"set_default_menu",
				bwn::wrap_callback(this, &MenuManager::resetDefaultMenu));
			database->addMethod(U"pop_last_menu", bwn::wrap_callback([this]() { this->popLast(); }));
			database->addMethod(
				U"print_menu_stack",
				bwn::callback<void()>([this]()
				{
					BWN_LOG("Menu stack:");
					BWN_LOG("> Default menu: {}", bwn::debug::getNodePath(m_default));
					for (const Menu*const menu : m_stack)
					{
						BWN_LOG("> Stack menu: {}", bwn::debug::getNodePath(menu));
					}
				}));
#endif // DEBUG_ENABLED && MODULE_BWN_CONSOLE_ENABLED
		}
		break;

		case NOTIFICATION_EXIT_TREE:
		{
			// Abort any operations if exists.
			if (m_context->isRunning())
			{
				m_context->abort();
				m_context->detach();
			}
			
			BWN_ASSERT_WARNING_COND(m_stack.empty(), "Menu stack in the menu manager should be empty on manager deletion, some menues wasn't poped.");
			if (Menu*const current = getCurrentMenu())
			{
				current->hide();
			}
			
			eraseAllMenu();

			if (getInstance() == this)
			{
				clearSingleton();
			}

#if defined(DEBUG_ENABLED) && defined(MODULE_BWN_CONSOLE_ENABLED)
			const auto database = ConsoleDatabase::getInstance();
			database->removeMethod(BWN_STRING_ID("push_menu"));
			database->removeMethod(BWN_STRING_ID("pop_till_menu"));
			database->removeMethod(BWN_STRING_ID("pop_list_menu"));
			database->removeMethod(BWN_STRING_ID("pop_specific_menu"));
			database->removeMethod(BWN_STRING_ID("set_default_menu"));
			database->removeMethod(BWN_STRING_ID("pop_last_menu"));
			database->removeMethod(BWN_STRING_ID("print_menu_stack"));
#endif // DEBUG_ENABLED && MODULE_BWN_CONSOLE_ENABLED
		}
		break;
	}

}

void bwn::MenuManager::push(Menu*const _menu)
{
	BWN_ASSERT_WARNING_COND(_menu != nullptr, "Can't push menu which is nullptr.");
	if (_menu == nullptr)
	{
		return;
	}

	pushChangingMenuOp(getCurrentMenu(), _menu);

	m_stack.push_back(_menu);
	_menu->addedToStack(this);
}

void bwn::MenuManager::popLast()
{
	// No reason to pop menu if it's not on the stack, we can't pop the default menu.
	if (m_stack.empty())
	{
		return;
	}

	Menu*const lastMenu = m_stack.back();
	Menu*const prelastMenu = m_stack.size() > 1
		? m_stack[m_stack.size() - 2]
		: m_default;

	pushChangingMenuOp(lastMenu, prelastMenu);

	lastMenu->removeFromStack();
	m_stack.pop_back();
}

void bwn::MenuManager::popTill(Menu*const _menuToPush)
{
	BWN_ASSERT_WARNING_COND(_menuToPush != nullptr, "Can't pop till nullptr menu, because there is no final point of the pop-list..");
	if (_menuToPush == nullptr)
	{
		return;
	}

	// This is the iterator of the last menu on the stack which will be poped.
	// By default we think that we are poping till default menu, and so all menues should be poped (aka last menu to pop is the last menu on the stack).
	MenuStack::iterator lastMenuToPopIt = m_stack.begin();

	if (_menuToPush != m_default)
	{
		const MenuStack::iterator menuIt = std::find(m_stack.begin(), m_stack.end(), _menuToPush);
		BWN_ASSERT_WARNING_COND(menuIt != m_stack.end(), "Can't pop menus till one, which is not added to the set. Menu: {}.", debug::getNodePath(_menuToPush));
		if (menuIt == m_stack.end())
		{
			return;
		}

		lastMenuToPopIt = menuIt + 1;

		if (lastMenuToPopIt == m_stack.end())
		{
			// This happens if we trying to pop till first menu in the stack,
			// so basically we have no menues to pop at all.
			return;
		}		
	}

	if (m_stack.empty())
	{
		// If we trying to pop till default menu, but the stack is empty, then do nothing.
		return;
	}

	// Basically menu which will be actually poped.
	Menu*const menuToPop = m_stack.back();	
	pushChangingMenuOp(menuToPop, _menuToPush);

	eraseStackMenu(lastMenuToPopIt, m_stack.end());
}

void bwn::MenuManager::popList(Menu*const _lastMenuToPop)
{
	BWN_ASSERT_WARNING_COND(_lastMenuToPop != nullptr, "Can't pop list till nullptr menu, because there is no final point of the pop-list.");
	
	if (_lastMenuToPop == nullptr)
	{
		return;
	}

	if (_lastMenuToPop == m_default)
	{
		pushChangingMenuOp(getCurrentMenu(), nullptr);
		eraseAllMenu();
	}
	else
	{
		const MenuStack::iterator lastMenuToPopIt = std::find(m_stack.begin(), m_stack.end(), _lastMenuToPop);
		BWN_ASSERT_WARNING_COND(lastMenuToPopIt != m_stack.end(), "Can't pop list menus till one, which is not pushed to the manager. Menu: {}.", debug::getNodePath(_lastMenuToPop));
		if (lastMenuToPopIt == m_stack.end())
		{
			return;
		}

		Menu*const menuToPush = lastMenuToPopIt == m_stack.begin()
			? m_default
			: *(lastMenuToPopIt - 1);
		Menu*const menuToPop = m_stack.back();

		pushChangingMenuOp(menuToPop, menuToPush);

		eraseStackMenu(lastMenuToPopIt, m_stack.end());
	}
}

void bwn::MenuManager::popSpecific(Menu*const _menu)
{
	BWN_ASSERT_WARNING_COND(_menu != nullptr, "Can't pop specific menu, because it's nullptr.");
	
	if (_menu == nullptr)
	{
		return;
	}

	if (_menu == m_default)
	{
		if (m_stack.empty())
		{
			pushChangingMenuOp(m_default, nullptr);
		}

		m_default->removeFromStack();
		m_default = nullptr;
	}
	else
	{
		const MenuStack::iterator menuIt = std::find(m_stack.begin(), m_stack.end(), _menu);
		BWN_ASSERT_WARNING_COND(menuIt != m_stack.end(), "Can't pop specific menu, because it's no on the stack. Menu: {}.", debug::getClassName(_menu));
		if (menuIt == m_stack.end())
		{
			return;
		}

		if (menuIt == (m_stack.end() - 1))
		{
			// If this is basically the last menu, then we simply poping last menu.
			popLast();
			return;
		}

		// If this is not last menu, then there is no reason to create any operations.
		(*menuIt)->removeFromStack();
		m_stack.erase(menuIt);
	}
}

void bwn::MenuManager::popSpecificFast(Menu*const _menu)
{
	BWN_ASSERT_WARNING_COND(_menu != nullptr, "Can't pop specific menu, because it's nullptr.");
	
	if (_menu == nullptr)
	{
		return;
	}

	if (_menu == m_default)
	{
		if (m_stack.empty())
		{
			clearOperationContext();
			m_default->forceHideMenu();
		}

		m_default->removeFromStack();
		m_default = nullptr;
	}
	else
	{
		const MenuStack::iterator menuIt = std::find(m_stack.begin(), m_stack.end(), _menu);
		BWN_ASSERT_WARNING_COND(menuIt != m_stack.end(), "Can't pop specific menu, because it's no on the stack. Menu: {}.", debug::getClassName(_menu));
		if (menuIt == m_stack.end())
		{
			return;
		}

		if (menuIt == (m_stack.end() - 1))
		{
			Menu*const menuToShow = m_stack.size() > 1
				? m_stack[m_stack.size() - 2]
				: m_default;
			if (menuToShow != nullptr)
			{
				pushChangingMenuOp(nullptr, menuToShow);
			}

			_menu->forceHideMenu();
			_menu->removeFromStack();
			m_stack.pop_back();
			return;
		}

		// If this is not last menu, then there is no reason to create any operations.
		(*menuIt)->removeFromStack();
		m_stack.erase(menuIt);
	}
}

void bwn::MenuManager::popAllStack()
{
	if (m_stack.empty())
	{
		// Nothing to pop.
		return;
	}

	Menu*const menuToPop = m_stack.back();
	Menu*const menuToPush = m_default;

	eraseStackMenu(m_stack.begin(), m_stack.end());

	pushChangingMenuOp(menuToPop, menuToPush);
}

void bwn::MenuManager::resetDefaultMenu(Menu* _menu)
{
	if (_menu == m_default)
	{
		return;
	}

	if (m_stack.empty())
	{
		// If stack is empty, then default menu is the one showed right now.
		pushChangingMenuOp(m_default, _menu);
	}

	if (m_default != nullptr)
	{
		m_default->removeFromStack();
	}

	m_default = _menu;

	if (m_default != nullptr)
	{
		m_default->addedToStack(this);
	}
}

bwn::Menu* bwn::MenuManager::getDefaultMenu() const
{
	return m_default;
}

bwn::Menu* bwn::MenuManager::getCurrentMenu() const
{
	return m_stack.empty()
		? m_default
		: m_stack.back();
}

void bwn::MenuManager::pushChangingMenuOp(Menu*const _menuToHide, Menu*const _menuToShow)
{
	// We expect at least on of those menues to be not nullptr.
	// Because of this we will not add actual check, but, just to be safe, we will add assert.
	BWN_ASSERT_ERROR_COND(
		_menuToHide != nullptr || _menuToShow != nullptr, 
		"This is not advised to create changing menu operation, if both menus are nullptr. This will simply abort context for nothing.");

	BWN_ASSERT_WARNING_COND(
		_menuToHide == nullptr || _menuToHide->is_inside_tree(),
		"This is ub to hide menu if it is not in the tree at the moment. Menu to show: {}. Menu to hide: {}.",
		debug::getNodePath(_menuToShow),
		debug::getNodePath(_menuToHide));

	BWN_ASSERT_WARNING_COND(
		_menuToShow == nullptr || _menuToShow->is_inside_tree(), 
		"This is ub to show menu if it is not in the tree at the moment. Menu to show: {}. Menu to hide: {}.",
		debug::getNodePath(_menuToShow),
		debug::getNodePath(_menuToHide));

	clearOperationContext();

	Ref<ChangeMenuOperation> changeOp = instantiateRef<ChangeMenuOperation>(_menuToHide, _menuToShow);
	m_channel.addOperation(changeOp, m_context);
}

void bwn::MenuManager::eraseStackMenu(const MenuStack::iterator _beginIt, const MenuStack::iterator _endIt)
{
	const MenuStack::reverse_iterator rbegin = MenuStack::reverse_iterator( _endIt );
	const MenuStack::reverse_iterator rend = MenuStack::reverse_iterator( _beginIt );
	for (MenuStack::reverse_iterator menuIt = rbegin; menuIt != rend; ++menuIt)
	{
		(*menuIt)->removeFromStack();
	}

	m_stack.erase(_beginIt, _endIt);
}

void bwn::MenuManager::eraseAllMenu()
{
	for (MenuStack::reverse_iterator menuIt = m_stack.rbegin(); menuIt != m_stack.rend(); ++menuIt)
	{
		(*menuIt)->removeFromStack();
	}
	m_stack.clear();

	if (m_default != nullptr)
	{
		m_default->removeFromStack();
		m_default = nullptr;
	}
}

void bwn::MenuManager::clearOperationContext()
{
	if (m_context->isRunning())
	{
		m_context->abort();
		m_context->detach();
	}
}

void bwn::MenuManager::resetDefaultMenuGeneric(Node* _menu)
{
	Menu*const menu = cast_to<Menu>(_menu);
	BWN_ASSERT_WARNING_COND(_menu == nullptr || menu != nullptr, "Can't reset default menu, because it's not an Menu node. Actual node class: {}.", debug::getClassName(_menu));

	if (_menu == nullptr || menu != nullptr)
	{
		// We must avoid reseting menu, only if cast failed. We could intentionally set nullptr.
		resetDefaultMenu(menu);
	}
}

void bwn::MenuManager::pushGeneric(Node* _menu)
{
	Menu*const menu = cast_to<Menu>(_menu);
	BWN_ASSERT_WARNING_COND(_menu == nullptr || menu != nullptr, "Can't push new menu, because it's not an Menu node. Actual node class: {}.", debug::getClassName(_menu));

	if (_menu == nullptr || menu != nullptr)
	{
		// We must avoid reseting menu, only if cast failed. We could intentionally set nullptr.
		push(menu);
	}
}

void bwn::MenuManager::popTillGeneric(Node* _menu)
{
	Menu*const menu = cast_to<Menu>(_menu);
	BWN_ASSERT_WARNING_COND(_menu == nullptr || menu != nullptr, "Can't pop till menu, because this menu is not an Menu node. Actual node class: {}.", debug::getClassName(_menu));

	if (_menu == nullptr || menu != nullptr)
	{
		// We must avoid reseting menu, only if cast failed. We could intentionally set nullptr.
		popTill(menu);
	}
}

void bwn::MenuManager::popListGeneric(Node* _menu)
{
	Menu*const menu = cast_to<Menu>(_menu);
	BWN_ASSERT_WARNING_COND(_menu == nullptr || menu != nullptr, "Can't pop list till menu, because it's not an Menu node. Actual node class: {}.", debug::getClassName(_menu));

	if (_menu == nullptr || menu != nullptr)
	{
		// We must avoid reseting menu, only if cast failed. We could intentionally set nullptr.
		popList(menu);
	}
}

void bwn::MenuManager::popSpecificGeneric(Node* _menu)
{
	Menu*const menu = cast_to<Menu>(_menu);
	BWN_ASSERT_WARNING_COND(_menu == nullptr || menu != nullptr, "Can't push new menu, because it's not an Menu node. Actual node class: {}.", debug::getClassName(_menu));

	if (_menu == nullptr || menu != nullptr)
	{
		// We must avoid reseting menu, only if cast failed. We could intentionally set nullptr.
		popSpecific(menu);
	}
}
#include "precompiled_ui.hpp"

#include "bwn_ui/menus/menu.hpp"
#include "bwn_ui/menus/menuManager.hpp"
#include "bwn_ui/menus/operations/hideMenuOperation.hpp"
#include "bwn_ui/menus/operations/showMenuOperation.hpp"

#include "bwn_coroutines/coroutineContext.hpp"
#include "bwn_coroutines/coroutineSemaphore.hpp"

namespace names
{
	static constexpr const char* k_onStartShowingSignal = "on_menu_start_showing";
	static constexpr const char* k_onStartHidingSignal = "on_menu_start_hiding";
	static constexpr const char* k_onShownSignal = "on_menu_shown";
	static constexpr const char* k_onHiddenSignal = "on_menu_hidden";
} // namespace names

bwn::Menu::Menu()
	: m_visualOperationsChannel()
	, m_manager( nullptr )
	, m_showAnimationPlayer( BWN_ONLY_IN_DEBUG("show menu player") )
	, m_hideAnimationPlayer( BWN_ONLY_IN_DEBUG("hide menu player") )
	, m_isDefaultMenu( false )
{
	m_visualOperationsChannel.instantiate();
	m_visualOperationsChannel->setSemaphore(bwn::instantiateRef<CoroutineSemaphore>(1));
}

bwn::Menu::~Menu()
{
	stopAnyOperations();
}

void bwn::Menu::_bind_methods()
{
	bwn::bindMethod(D_METHOD("push_menu"), &Menu::pushMenu);
	bwn::bindMethod(D_METHOD("pop_menu"), &Menu::popMenu);

	bwn::addSignal<Menu>(MethodUtility<void()>(names::k_onStartShowingSignal).get());
	bwn::addSignal<Menu>(MethodUtility<void()>(names::k_onStartHidingSignal).get());

	bwn::addSignal<Menu>(MethodUtility<void()>(names::k_onShownSignal).get());
	bwn::addSignal<Menu>(MethodUtility<void()>(names::k_onHiddenSignal).get());

	BWN_BIND_PROPERTY("is_default_menu", &Menu::setIsDefaultMenu, &Menu::getIsDefaultMenu);

	BWN_BIND_PROPERTY(
		"show_menu_player",
		&Menu::setShowAnimationPlayerPath,
		&Menu::getShowAnimationPlayerPath,
		PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		AnimationPlayer::get_class_static());

	BWN_BIND_PROPERTY(
		"hide_menu_player",
		&Menu::setHideAnimationPlayerPath,
		&Menu::getHideAnimationPlayerPath,
		PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		AnimationPlayer::get_class_static());
}

void bwn::Menu::_notification(const int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_ENTER_TREE:
		{
			m_showAnimationPlayer.onEnterTree(*this);
			m_hideAnimationPlayer.onEnterTree(*this);

#if defined(DEBUG_ENABLED)
			m_visualOperationsChannel->setParentCreatorInfo(
				std::make_shared<bwn::debug::CoroutineCreatorInfo>(
					bwn::format("{}", get_path())));
#endif
			if (!isEditor() && m_isDefaultMenu)
			{
				const auto manager = MenuManager::getInstance();

#if defined(DEBUG_ENABLED)
				const Menu*const currentDefault = manager->getDefaultMenu();

				BWN_ASSERT_WARNING_COND(
					currentDefault == nullptr || currentDefault == this,
					"We are overriding default menu from '{}', to '{}'.",
					debug::getNodePath(currentDefault),
					debug::getNodePath(this));
#endif // DEBUG_ENABLED

				manager->resetDefaultMenu(this);
			}
		}
		break;

		case NOTIFICATION_EXIT_TREE:
		{
#if defined(DEBUG_ENABLED)
			m_visualOperationsChannel->setParentCreatorInfo(
				std::make_shared<bwn::debug::CoroutineCreatorInfo>(L"Menu outside of a tree"));
#endif

			// If we are exiting tree, we want to stop any operations currently showing.
			stopAnyOperations();

			if (m_manager != nullptr)
			{
				m_manager->popSpecificFast(this);
			}
		}
		break;
	}
}

void bwn::Menu::addedToStack(MenuManager*const _manager)
{
	m_manager = _manager;
}

void bwn::Menu::removeFromStack()
{
	m_manager = nullptr;
}

void bwn::Menu::showMenu(const Ref<CoroutineContext>& _context)
{
	// If any last operations are not done yet, we should abort them.
	stopAnyOperations();

	show();

	onStartShowingV();
	emit_signal(SNAME(names::k_onStartShowingSignal));

	// This is final operation. which will be called last, and so it will signal that showing of the menu is finished.
	m_visualOperationsChannel->addOperation(
		bwn::instantiateRef<ShowMenuOperation>(*this),
		_context);
}
void bwn::Menu::hideMenu(const Ref<CoroutineContext>& _context)
{
	// If any operations are in progress, we wan't to cancel those operations.
	stopAnyOperations();
	
	onStartHidingV();
	emit_signal((names::k_onStartHidingSignal));

	// This is final operation. which will be called last, and so it will signal that hiding of the menu is finished.
	m_visualOperationsChannel->addOperation(
		bwn::instantiateRef<HideMenuOperation>(*this),
		_context);
}

void bwn::Menu::forceHideMenu()
{
	stopAnyOperations();

	onStartHidingV();
	emit_signal(SNAME(names::k_onStartHidingSignal));

	if (AnimationPlayer*const hidePlayer = m_hideAnimationPlayer.getNode())
	{
		hidePlayer->play("hide");
		const float advanceDelta = hidePlayer->get_current_animation_length() - hidePlayer->get_current_animation_position();
		hidePlayer->advance(advanceDelta);
	}

	onMenuHideOperationDone();
}

void bwn::Menu::forceShowMenu()
{
	show();

	onStartShowingV();
	emit_signal(SNAME(names::k_onStartShowingSignal));

	if (AnimationPlayer*const showPlayer = m_showAnimationPlayer.getNode())
	{
		showPlayer->play("show");
		const float advanceDelta = showPlayer->get_current_animation_length() - showPlayer->get_current_animation_position();
		showPlayer->advance(advanceDelta);
	}

	onMenuShowOperationDone();
}

void bwn::Menu::stopAnyOperations()
{
	m_visualOperationsChannel->abortOperations();
}

void bwn::Menu::pushMenu()
{
	if (isOnStack())
	{
		return;
	}

	const auto manager = MenuManager::getInstance();
	manager->push(this); 
}

void bwn::Menu::popMenu()
{
	if (isOnStack())
	{
		m_manager->popSpecific(this);
		return;
	}
}

bool bwn::Menu::isOnStack() const
{
	return m_manager != nullptr;
}

void bwn::Menu::setIsDefaultMenu(const bool _isDefault)
{
	m_isDefaultMenu = _isDefault;

	if (!isEditor() && is_inside_tree())
	{
		const auto manager = MenuManager::getInstance();
		Menu*const currentDefault = manager->getDefaultMenu();
		if (m_isDefaultMenu)
		{
			BWN_ASSERT_WARNING_COND(
				currentDefault == nullptr || currentDefault == this,
				"We are overriding default menu from '{}', to '{}'.",
				debug::getNodePath(currentDefault),
				debug::getNodePath(this));

			manager->resetDefaultMenu(this);
		}
		else
		{
			if (currentDefault == this)
			{
				manager->resetDefaultMenu(nullptr);
			}
		}
	}
}

bool bwn::Menu::getIsDefaultMenu() const
{
	return m_isDefaultMenu;
}

AnimationPlayer* bwn::Menu::getShowAnimationPlayer() const
{
	return m_showAnimationPlayer.getNode();
}

AnimationPlayer* bwn::Menu::getHideAnimationPlayer() const
{
	return m_hideAnimationPlayer.getNode();
}

const NodePath& bwn::Menu::getShowAnimationPlayerPath() const
{
	return m_showAnimationPlayer.getPath();
}

void bwn::Menu::setShowAnimationPlayerPath(const NodePath& _path)
{
	m_showAnimationPlayer.setNode(*this, _path);
}

const NodePath& bwn::Menu::getHideAnimationPlayerPath() const
{
	return m_hideAnimationPlayer.getPath();
}

void bwn::Menu::setHideAnimationPlayerPath(const NodePath& _path)
{
	m_hideAnimationPlayer.setNode(*this, _path);
}

void bwn::Menu::onStartShowingV()
{}

void bwn::Menu::onStartHidingV()
{}

void bwn::Menu::onShownV()
{}

void bwn::Menu::onHiddenV()
{}

void bwn::Menu::onMenuShowOperationDone()
{
	onShownV();
	emit_signal(SNAME(names::k_onShownSignal));
}

void bwn::Menu::onMenuHideOperationDone()
{
	onHiddenV();
	emit_signal(SNAME(names::k_onHiddenSignal));

	hide();
}

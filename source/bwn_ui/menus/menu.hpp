#pragma once

#include "bwn_coroutines/coroutineChannel.hpp"
#include "bwn_core/types/nodeProperty.hpp"

#include <scene/gui/control.h>

class AnimationPlayer;

namespace bwn
{
	class MenuManager;
	class CoroutineContext;
	class CoroutineSemaphore;
	class ShowMenuOperation;
	class HideMenuOperation;

	class Menu : public Control
	{
		GDCLASS(Menu, Control);

		friend ShowMenuOperation;
		friend HideMenuOperation;

		//
		// Construction and destruction.
		//
	public:
		Menu();
		virtual ~Menu() override;

		//
		// Godot method.
		//
	private:
		static void _bind_methods();
		void _notification(const int _notification);

		//
		// Public interface.
		//
	public:
		// Called then menu is pushed into stack.
		void addedToStack(MenuManager*const _manager);
		// Called just before menu is poped from the stack.
		void removeFromStack();
		// Called then menu shoud be shown.
		// The context can be used to push specifc operation, which will show some animation etc.
		void showMenu(const Ref<CoroutineContext>& _context);
		// Called then menu shoud be hidden.
		// The context can be used to push specifc operation, which will show some animation etc.
		void hideMenu(const Ref<CoroutineContext>& _context);
		// Quickly synchronously hides menu.
		void forceHideMenu();
		// Quickly synchronously shows menu.
		void forceShowMenu();
		// Aborts any operations which were pushed on showing/hiding menu.
		void stopAnyOperations();
		// Tries to push that menu to manager.
		void pushMenu();
		// Tries to pop this menu from the manager.
		void popMenu();
		// Returnes true if menu was pushed to the stack.
		bool isOnStack() const;
		// Sets this menu to be default menu.
		void setIsDefaultMenu(const bool _isDefault);
		// Returns if this menu should be the default one (doesn't actually checks in MenuManager if this menu actually set upped as default.)
		bool getIsDefaultMenu() const;
		// Return animation player which is used to show this specific menu.
		AnimationPlayer* getShowAnimationPlayer() const;
		// Return animation player which is used to hide this specific menu.
		AnimationPlayer* getHideAnimationPlayer() const;

		// Getters/Setters for the show animation player property.
		const NodePath& getShowAnimationPlayerPath() const;
		void setShowAnimationPlayerPath(const NodePath& _path);
		// Getters/Setters for the hide animation player property.
		const NodePath& getHideAnimationPlayerPath() const;
		void setHideAnimationPlayerPath(const NodePath& _path);

		//
		// Protected members.
		//
	protected:
		// This callback called in the beginning of menu showing.
		virtual void onStartShowingV();
		// This callback called in the beginning of menu showing.
		virtual void onStartHidingV();

		// This callback called at the end of menu showing.
		virtual void onShownV();
		// This callback called at the end of menu showing.
		virtual void onHiddenV();

		//
		// Private members.
		//
	private:
		// Those methods basically called from ShowMenuOperation and HideMenuOperation directly.
		void onMenuShowOperationDone();
		void onMenuHideOperationDone();

		//
		// Private members.
		//
	private:
		// This is channel used for showing/hiding menus operations
		Ref<CoroutineChannelObject> m_visualOperationsChannel;
		// Menu manager to which this menu was pushed. If menu is not on the manager stack this variable is nullptr.
		MenuManager* m_manager;
		// Animation which is used to show menu.
		NodeProperty<AnimationPlayer, Menu, void> m_showAnimationPlayer;
		// Animation which is used to hide menu.
		NodeProperty<AnimationPlayer, Menu, void> m_hideAnimationPlayer;
		// Should this menu try to be default menu.
		bool m_isDefaultMenu;
	};
} // namespace bwn
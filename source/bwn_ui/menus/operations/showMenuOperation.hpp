#pragma once 

#include "bwn_coroutines/coroutineOperation.hpp"

class AnimationPlayer;

namespace bwn
{
	class CoroutineChannel;
	class Menu;

	// This is mostly just a stub operation, which will
	class ShowMenuOperation : public CoroutineOperation
	{
		GDCLASS(ShowMenuOperation, CoroutineOperation);

		//
		// Construction and destruction.
		//
	public:
		ShowMenuOperation();
		ShowMenuOperation(Menu& _menu);
		virtual ~ShowMenuOperation() override;

		//
		// Protected members.
		//
	protected:
		// Basically we will call 'hide' in the very beginning.
		virtual void onStartedV() override;
		// Just checking if we are still playing the animation.
		virtual void onUpdateV(const float) override;
		// Called in the end of animation (or right after the start). Basically just alerts menu that operation has finished.
		virtual void onFinishedV() override;
		// Same as onFinishedV because abort of visual operations counts as fast finish.
		virtual void onAbortedV() override;

		//
		// Private members.
		//
	private:
		// Menu which should be shown.
		Menu* m_menu = nullptr;
		AnimationPlayer* m_player = nullptr;
	};
} // namespace bwn
#include "precompiled_ui.hpp"

#include "bwn_ui/menus/operations/hideMenuOperation.hpp"
#include "bwn_ui/menus/menu.hpp"

#include "bwn_ui/menus/notifications/menuChangedNotification.hpp"

bwn::HideMenuOperation::HideMenuOperation()
	: m_menu( nullptr )
	, m_player( nullptr )
{}

bwn::HideMenuOperation::HideMenuOperation(Menu& _menu)
	: CoroutineOperation( BWN_STRING_ID("HideMenuOperation") )
	, m_menu( &_menu )
	, m_player( nullptr )
{}

bwn::HideMenuOperation::~HideMenuOperation() = default;

void bwn::HideMenuOperation::onStartedV()
{
	BWN_TRAP_COND(m_menu != nullptr, "Incorrectly initialized HideMenuOperation. It was probably called not from Menu.");

	m_player = m_menu->getHideAnimationPlayer();
	if (m_player != nullptr)
	{
		// Just so animation will be visible, we will forcefully show menu if it's hidden.
		// But we must do this only if we have player, otherwise there is no reason to flick menu visibility for one frame.
		if (!m_menu->is_visible())
		{
			m_menu->show();
		}

		m_player->play("hide");
	}
	else
	{
		exitSucceeded();
	}
}

void bwn::HideMenuOperation::onUpdateV(const float)
{
	if (!m_player->is_playing())
	{
		exitSucceeded();
	}
}

void bwn::HideMenuOperation::onFinishedV()
{
	m_menu->onMenuHideOperationDone();
	bwn::signalNotification<MenuChangedNotification>(m_menu, m_menu, false, false);
}

void bwn::HideMenuOperation::onAbortedV()
{
	m_menu->onMenuHideOperationDone();
	bwn::signalNotification<MenuChangedNotification>(m_menu, m_menu, false, true);
}
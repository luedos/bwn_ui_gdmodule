#include "precompiled_ui.hpp"

#include "bwn_ui/menus/operations/changeMenuOperation.hpp"
#include "bwn_ui/menus/menu.hpp"

#include "bwn_coroutines/coroutineContext.hpp"
#include "bwn_coroutines/coroutineResult.hpp"

bwn::ChangeMenuOperation::ChangeMenuOperation()
	: CoroutineOperation( BWN_STRING_ID("ChangeMenuOperation") )
	, m_hideMenu( nullptr )
	, m_showMenu( nullptr )
	, m_firstMenuHidden( false )
{}

bwn::ChangeMenuOperation::ChangeMenuOperation(Menu*const _menuToHide, Menu*const _menuToShow)
	: CoroutineOperation( BWN_STRING_ID("ChangeMenuOperation") )
	, m_context()
	, m_hideMenu(_menuToHide)
	, m_showMenu(_menuToShow)
	, m_firstMenuHidden( false )
{
	BWN_ASSERT_WARNING_COND(m_hideMenu != nullptr || m_showMenu != nullptr, "This is not preferable to create ChangeMenuOperation without any menus to change.");
	m_context.instantiate();
}

bwn::ChangeMenuOperation::~ChangeMenuOperation()
{
	m_context->setCallback(nullptr);
}

void bwn::ChangeMenuOperation::onStartedV()
{
	BWN_TRAP_COND(m_context.is_valid(), "The coroutine context wasn't initialized. The operation was probably created outside of Menu class.");

	m_firstMenuHidden = m_hideMenu == nullptr;
	if (m_hideMenu != nullptr)
	{
		m_context->setCallback([this](Ref<CoroutineContext> _context){ this->onHidingComplete(_context); });
		m_hideMenu->hideMenu(m_context);
	}
	else if (m_showMenu != nullptr)
	{
		m_context->setCallback([this](Ref<CoroutineContext> _context){ this->onShowingComplete(_context); });
		m_showMenu->showMenu(m_context);
	}
	else
	{
		exitSucceeded();
	}
}

void bwn::ChangeMenuOperation::onAbortedV()
{
	// Abort whatever operation is currently running.
	// This will also finish animation player track (then ever this is hiding/showing of the menu).
	m_context->abort();

	if (m_firstMenuHidden && m_showMenu != nullptr)
	{
		m_showMenu->forceShowMenu();
	}
}

void bwn::ChangeMenuOperation::onHidingComplete(Ref<CoroutineContext> _context)
{
	m_firstMenuHidden = true;
	Ref<CoroutineResult> result = _context->getResult();
	if (result->isFailed())
	{
		m_hideMenu->hide();
	}

	if (m_showMenu != nullptr)
	{
		m_context->setCallback([this](Ref<CoroutineContext> _context){ this->onShowingComplete(_context); });
		m_showMenu->showMenu(m_context);
	}
	else
	{
		exitSucceeded();
	}
}

void bwn::ChangeMenuOperation::onShowingComplete(Ref<CoroutineContext> _context)
{
	Ref<CoroutineResult> result = _context->getResult();
	if (result->isSucceeded())
	{
		exitSucceeded();
	}
	else
	{
		setResult(Error::FAILED, bwn::format("Failed to open menu because: {}", result->getSystemResult().getErrorDescription()));
	}
}
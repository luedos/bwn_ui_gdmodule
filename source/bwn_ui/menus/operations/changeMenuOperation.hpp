#pragma once

#include "bwn_coroutines/coroutineOperation.hpp"

namespace bwn
{
	
	class CoroutineContext;
	class Menu;

	// This operation will return negative result only if second menu wasn't shown properly.
	// If first menu failed to hide, we will simply call force 'hide' on it, 
	// but if second menu will fail to show, this operation will fail as well.
	class ChangeMenuOperation : public CoroutineOperation
	{
		GDCLASS(ChangeMenuOperation, CoroutineOperation);

		//
		// Construction and destruction.
		//
	public:
		ChangeMenuOperation();
		ChangeMenuOperation(Menu*const _menuToHide, Menu*const _menuToShow);
		virtual ~ChangeMenuOperation() override;

		//
		// Protected members.
		//
	protected:
		virtual void onStartedV() override;
		virtual void onAbortedV() override;

		//
		// Private methods.
		//
	private:
		// Called then first menu is fully hidden.
		void onHidingComplete(Ref<CoroutineContext> _context);
		// Called then second menu is fully shown;
		void onShowingComplete(Ref<CoroutineContext> _context);

		//
		// Private members.
		//
	private:
		Ref<CoroutineContext> m_context;
		Menu* m_hideMenu = nullptr;
		Menu* m_showMenu = nullptr;
		bool m_firstMenuHidden = false;
	};

} // namespace bwn
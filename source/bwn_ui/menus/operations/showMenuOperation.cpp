#include "precompiled_ui.hpp"

#include "bwn_ui/menus/operations/showMenuOperation.hpp"
#include "bwn_ui/menus/menu.hpp"

#include "bwn_ui/menus/notifications/menuChangedNotification.hpp"

bwn::ShowMenuOperation::ShowMenuOperation()
	: m_menu( nullptr )
	, m_player( nullptr )
{}

bwn::ShowMenuOperation::ShowMenuOperation(Menu& _menu)
	: CoroutineOperation( BWN_STRING_ID("ShowMenuOperation") )
	, m_menu( &_menu )
	, m_player( nullptr )
{}

bwn::ShowMenuOperation::~ShowMenuOperation() = default;

void bwn::ShowMenuOperation::onStartedV()
{
	BWN_TRAP_COND(m_menu != nullptr, "Incorrectly initialized ShowMenuOperation. It was probably called not from Menu.");

	m_player = m_menu->getShowAnimationPlayer();
	if (m_player != nullptr)
	{
		m_player->play("show");
	}
	else
	{
		// If there is no animation player, simply exit.
		exitSucceeded();
	}
}

void bwn::ShowMenuOperation::onUpdateV(const float)
{
	if (!m_player->is_playing())
	{
		exitSucceeded();
	}
}

void bwn::ShowMenuOperation::onFinishedV()
{
	m_menu->onMenuShowOperationDone();
	bwn::signalNotification<MenuChangedNotification>(m_menu, m_menu, true, false);
}

void bwn::ShowMenuOperation::onAbortedV()
{
	if (m_player != nullptr && m_player->is_playing())
	{
		const float advanceDelta = m_player->get_current_animation_length() - m_player->get_current_animation_position();
		m_player->advance(advanceDelta);
	}
	m_menu->onMenuShowOperationDone();
	bwn::signalNotification<MenuChangedNotification>(m_menu, m_menu, true, true);
}
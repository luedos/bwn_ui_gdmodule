#pragma once 

#include "bwn_coroutines/coroutineOperation.hpp"

class AnimationPlayer;

namespace bwn
{
	class Menu;

	// This is mostly just a stub operation, which will
	class HideMenuOperation : public CoroutineOperation
	{
		GDCLASS(HideMenuOperation, CoroutineOperation);

		//
		// Construction and destruction.
		//
	public:
		HideMenuOperation();
		HideMenuOperation(Menu& _menu);
		virtual ~HideMenuOperation() override;

		//
		// Protected members.
		//
	protected:
		// Basically we will call 'hide' in the very beginning.
		virtual void onStartedV() override;
		// Just checking if we are still playing the animation.
		virtual void onUpdateV(const float) override;
		// Called in the end of animation (or right after the start). Basically just alerts menu that operation has finished.
		virtual void onFinishedV() override;
		// Same as onFinishedV because abort of visual operations counts as fast finish.
		virtual void onAbortedV() override;

		//
		// Private members.
		//
	private:
		// Menu which should be hidden.
		Menu* m_menu = nullptr;
		// Actual player used to hide the menu.
		AnimationPlayer* m_player = nullptr;
	};
} // namespace bwn
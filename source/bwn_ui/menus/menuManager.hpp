#pragma once

#include "bwn_core/types/singleton.hpp"
#include "bwn_coroutines/coroutineChannel.hpp"

#include <scene/main/node.h>
#include <core/string/node_path.h>

namespace bwn
{
	class Menu;
	class CoroutineContext;

	class MenuManager : public Node, public PlainSingleton<MenuManager>
	{
		GDCLASS(MenuManager, Node);

	private:
		using MenuStack = std::vector<Menu*>;

		//
		// Construction and destruction.
		//
	public:
		MenuManager();
		virtual ~MenuManager() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();
		void _notification(int _notification);

		//
		// Public interface.
		//
	public:
		// Pushes menu onto the stack, and replaces current menu.
		void push(Menu*const _menu);
		// Pops last menu from the stack. If there exist menu on the stack before it, pushes it. If not, pushes the default menu.
		void popLast();
		// Pops all menues till specific one, and pushes that specific menu. If menu not on the stack, no menues will be poped at all.
		void popTill(Menu*const _menuToPush);
		// Same as popTill, but that spesific menu is also poped. Also can pop the default menu.
		void popList(Menu*const _headMenu);
		// Pops specific menu from the stack, but doesn't pops menues after it. Also can pop the default menu if this is the one provided.
		void popSpecific(Menu*const _menu);
		// Pops specific menu without pushing operations for it. Can pop default menu if this is the one provided.
		void popSpecificFast(Menu*const _menu);
		// Pops all menues from the stack. If there is default menu, pushes the default menu.
		void popAllStack();
		// Sets default menu, which works as a basic one (aka can't be poped).
		void resetDefaultMenu(Menu*const _menu);
		// Returns current default menu.
		Menu* getDefaultMenu() const;
		// Returns current opened menu. (Can be nullptr if there is not even default menu)
		Menu* getCurrentMenu() const;

		//
		// Private methods.
		//
	private:
		// Creates and pushes operation to hide one menu, and show another.
		void pushChangingMenuOp(Menu*const _menuToHide, Menu*const _menuToShow);
		// Erases specific amount of menu from stack.
		void eraseStackMenu(const MenuStack::iterator _beginIt, const MenuStack::iterator _endIt);
		// Fully clears stack and default menu.
		void eraseAllMenu();
		// Clears operation context if any changing operation are running.
		void clearOperationContext();
		// This method is used mostly just for the binding of gdscript.
		void resetDefaultMenuGeneric(Node*const _menu);
		void pushGeneric(Node*const _menu);
		void popTillGeneric(Node*const _menuToPush);
		void popListGeneric(Node*const _headMenu);
		void popSpecificGeneric(Node*const _menu);

		//
		// Private members.
		//
	private:
		// The default menu, which can't be poped.
		Menu* m_default = nullptr;
		// Stack of all menues. The top one at the back of the vector.
		MenuStack m_stack;
		// This is channel, which is used to push operations for changing the menu.
		CoroutineChannel m_channel;
		// This context is used for menu switching.
		Ref<CoroutineContext> m_context;
	};
} // namespace bwn

add_lib(bwn-ui-lib 
	PARENT_LIB
		modules-lib
	COMPONENTS
	# godot default environments
		modules-env
		global-warnings-env
	# game project environment
		bwn-env
)

target_link_libraries(bwn-ui-lib PRIVATE
	bwn-core-lib
	bwn-coroutines-lib
	bwn-notifications-lib
)
if (godot_module_bwn_console_enabled)
	target_link_libraries(bwn-ui-lib PRIVATE bwn-console-lib)
endif()

# Setuping main lib data
target_include_directories(bwn-ui-lib PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/source")
target_include_directories(bwn-ui-lib PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/precompiled")
target_precompile_headers(bwn-ui-lib PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/precompiled/precompiled_ui.hpp")

# Including lib sources.
file(GLOB_RECURSE __SOURCES LIST_DIRECTORIES FALSE "*.cpp")
target_sources(bwn-ui-lib PRIVATE ${__SOURCES})
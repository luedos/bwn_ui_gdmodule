# so functions in the module would not be dependent on folder name at a configuration time.
get_filename_component(__MODULE_NAME "${CMAKE_CURRENT_LIST_DIR}" NAME)

function(${__MODULE_NAME}_configure_module)
	# pass, nothing to do here
endfunction()

function(${__MODULE_NAME}_get_module_can_build __OUTPUT)
	set(${__OUTPUT} TRUE PARENT_SCOPE)
endfunction()

function(${__MODULE_NAME}_get_module_dependencies __REQUIRED_DEPENDENCIES __OPTIONAL_DEPENDENCIES)
	set("${__REQUIRED_DEPENDENCIES}" "bwn_core" "bwn_coroutines" "bwn_notifications" PARENT_SCOPE)
	set("${__OPTIONAL_DEPENDENCIES}" "bwn_console" PARENT_SCOPE)
endfunction()


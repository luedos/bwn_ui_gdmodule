## Some ui additions for godot engine

This library introduce some additional ui features for godot.
The list of features:
- stack based menus

This modules is based on cmake build system implementation and not the scons one.

Other dependencies for this library include:
- bwn_core_gdmodule
- bwn_coroutines_gdmodule
- bwn_notifications_gdmodule